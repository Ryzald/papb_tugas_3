package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {
    ImageView alis,rambut,janggut, kumis;
    CheckBox cekAlis,cekRambut,cekJanggut,cekKumis;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        alis = findViewById(R.id.alis);
        rambut = findViewById(R.id.rambut);
        janggut = findViewById(R.id.janggut);
        kumis = findViewById(R.id.kumis);
        cekAlis = findViewById(R.id.cekAlis);
        cekRambut = findViewById(R.id.cekRambut);
        cekJanggut = findViewById(R.id.cekJanggut);
        cekKumis = findViewById(R.id.cekKumis);

        //     method cek alis
        cekAlis.setOnClickListener(new View.OnClickListener() {
            //        visibilitas
            boolean visible;

            @Override
            public void onClick(View view) {
                visible = !visible;
                alis.setVisibility(visible? view.VISIBLE : view.GONE);
            }
        });

//        method cek rambut
        cekRambut.setOnClickListener(new View.OnClickListener() {
            //            visibilitas
            boolean visible;

            @Override
            public void onClick(View view) {
                visible = !visible;
                rambut.setVisibility(visible? view.VISIBLE : view.GONE);
            }
        });

        //        method cek janggut
        cekJanggut.setOnClickListener(new View.OnClickListener() {
            //            visibilitas
            boolean visible;

            @Override
            public void onClick(View view) {
                visible = !visible;
                janggut.setVisibility(visible? view.VISIBLE : view.GONE);
            }
        });

//        method cek alis
        cekKumis.setOnClickListener(new View.OnClickListener() {
//            visibilitas
            boolean visible;

            @Override
            public void onClick(View view) {
                visible = !visible;
                kumis.setVisibility(visible? view.VISIBLE : view.GONE);
            }
        });
    }
}